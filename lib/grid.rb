require 'inject_with_index'

class Grid
  attr_accessor :cells, :rows, :cols, :boxes

  POSSIBLE_VALUES = (1..9).to_a

  def initialize str
    parsed_input = str.split(//).map(&:to_i)
    @cells = make_cells_from_string parsed_input
    @rows = make_rows
    @cols = @rows.transpose
    @boxes = make_boxes
    tell_cells_about_their_neighbours
  end

  def solve
    @cells.map do |cell|
      if cell.empty?
        new_values = (POSSIBLE_VALUES - cell.neighbours)
        cell.value = new_values.first if new_values.length == 1
      end
    end
    unless solved?
      remake_rows_cols_boxes_neighbours
      solve
    end
    cell_values
  end

  private
    def make_cells_from_string starting_array
      starting_array.inject_with_index([]) do |cells, val, i| 
        cells << Cell.new(val, i)
      end
    end

    def make_rows
      @cells.each_slice(9).inject([]) {|acc, slice| acc << slice}
    end
    
    def make_cols
      @cols = @rows.transpose
    end

    def make_boxes
      @cells.inject({}) do |h, cell| 
        h[cell.box] = h[cell.box] ? h[cell.box] << cell : [cell] ; h
      end.values
    end

    def tell_cells_about_their_neighbours
      @cells.map do |cell|
        cell.neighbours = (@rows[cell.row].map{|cell| cell.value} +
                           @cols[cell.col].map{|cell| cell.value} +
                           @boxes[cell.box].map{|cell| cell.value} - [0]).uniq
      end 
    end

    def remake_rows_cols_boxes_neighbours
      make_rows; make_cols; make_boxes; tell_cells_about_their_neighbours
    end

    def solved?
      @cells.all? {|cell| cell.value != 0}
    end

    def cell_values
      @cells.map{|c| c.value}
    end
end
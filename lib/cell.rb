class Cell
  attr_accessor :value, :row, :col, :box, :neighbours

  def initialize value, position
    @value = value
    @row = position / 9
    @col = position % 9
    @box = (@row / 3) * 3 + @col / 3
  end

  def empty?
    @value == 0
  end

end
require 'grid'
require 'cell'

describe Grid do
  let(:grid) { Grid.new "015003002000100906270068430490002017501040380003905000900081040860070025037204600"}
  
  it 'takes a string of 81 chars and turns it into an array of cells' do
    expect(grid.cells.length).to eq(81)
    expect(grid.cells).to be_a(Array)
  end

  it 'its cells are instances of Cell' do
    expect(grid.cells.first).to be_a(Cell)
  end

  it 'it knows whats in its rows' do
    expect(grid.rows.first.map{|cell| cell.value}).to eq [0,1,5,0,0,3,0,0,2]
  end

  it 'it knows whats in its cols' do
    expect(grid.cols.first.map{|cell| cell.value}).to eq [0,0,2,4,5,0,9,8,0]
  end

  it 'it knows whats in its boxes' do
    expect(grid.boxes.first.map{|cell| cell.value}).to eq [0,1,5,0,0,0,2,7,0]
  end

  it 'every cell knows its neighbours so to speak' do
    expect(grid.cells.first.neighbours).to eq (
                                                [0,1,5,0,0,3,0,0,2] + [0,0,2,4,5,0,9,8,0] + [0,1,5,0,0,0,2,7,0] - [0]
                                              ).uniq
  end

  it 'solves sudokus!' do
    expect(grid.solve).to eq(
                    [6, 1, 5, 4, 9, 3, 8, 7, 2, 3, 4, 8, 1, 2, 7, 9, 5, 6, 2, 7, 9, 5, 6, 8, 4, 3, 1, 4, 9, 6, 8, 3, 
                    2, 5, 1, 7, 5, 2, 1, 7, 4, 6, 3, 8, 9, 7, 8, 3, 9, 1, 5, 2, 6, 4, 9, 5, 2, 6, 8, 1, 7, 4, 3, 8, 
                    6, 4, 3, 7, 9, 1, 2, 5, 1, 3, 7, 2, 5, 4, 6, 9, 8]
                    )
  end

end
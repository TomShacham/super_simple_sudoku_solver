require 'cell'

describe Cell do

  it 'has a value' do
    cell = Cell.new(5, 0)
    expect(cell.value).to eq 5
  end

  it 'there4 knows if its empty' do
    cell = Cell.new(0,0)
    expect(cell.empty?).to be true
  end

  it 'knows what row its in' do
    cell = Cell.new(5,0)
    expect(cell.row).to eq 0
  end

  it 'knows what col its in' do
    cell = Cell.new(3,20)
    expect(cell.col).to eq 2
  end

  it 'knows what box its in' do
    cell = Cell.new(5,40)
    expect(cell.box).to eq 4
  end
end